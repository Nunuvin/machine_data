/*
 *  neofetch ripoff without cool graphics...
 *  written for subsys for win, so some stuff might not work... (DE? WM? what are those?)
 */
#include <stdio.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/utsname.h>
#include <stdlib.h> //getenv
#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <X11/Xlib.h>

struct mem{
    char *dim;
    unsigned long val;
};


void print_cpu(){
    FILE *fd;
    fd =fopen("/proc/cpuinfo","r");
   
    size_t sz = 0;
    char *lin = 0;
    char *model = "model name";
    do{
        ssize_t lsz = getline(&lin,&sz,fd);
        if(lsz<0) break;
        if(lsz>10){
            char subbuf[11];
            char *sb = (char *) &subbuf;
            memcpy(sb,lin,10);
            if(strncmp(sb,model,10)==0){
                printf("%s",lin);
                return;
            }
        }
    }while(!feof(fd));

    fclose(fd);
    
}

void print_terminal(){
    FILE *fd;
    pid_t p = getppid();
    
    char *spid = (char *)malloc(32*sizeof(char));
    sprintf(spid, "/proc/%u/comm",p);

    fd = fopen(spid,"r");
   
    size_t sz = 0;
    char *lin = 0;
    do{
        ssize_t lsz = getline(&lin,&sz,fd);
        if(lsz<0) break;
        printf("%s",lin);
    }while(!feof(fd));

    fclose(fd);
}

void convert_mem(unsigned long b, struct mem *m){
    int i = -1;
    int bold = 0;
    char *suf[] = {"B ","KB","MB","GB","TB"};
    do{
        bold = b;
        b = b/1024;
        i++;
    }while(b>0);
    m->dim = suf[i];
    m->val = bold;
}



int main(){

    Display *d = XOpenDisplay(NULL);
    struct utsname uts;
    struct sysinfo sinfo;
    struct mem mfree, mtot;
    struct passwd *pw;
    pw = getpwuid(geteuid());
    sysinfo(&sinfo);
    uname(&uts);

    convert_mem(sinfo.freeram, &mfree);
    convert_mem(sinfo.totalram, &mtot);
    printf("%s @ %s\n",pw->pw_name,uts.nodename);
    printf("OS: %s %s\n",uts.sysname,uts.machine); // get sys info uname does not tell which os
    printf("Kernel: %s\n",uts.release);
    printf("Uptime: %lu hours, %lu mins, %lu sec\n", sinfo.uptime/3600, (sinfo.uptime - (sinfo.uptime/3600))/60%60,
                                sinfo.uptime % 60);
//    printf("Shell: %s\n",getenv("SHELL")); //while works not guaranteed
    printf("Shell: %s\n", pw->pw_shell);
    if(d!=NULL) printf("Resolution: %d x %d\n", d->screens[DefaultScreen(d)].width, d->screens[DefaultScreen(d)].height); //no idea if works or not. magic of subsys...
    printf("Terminal: ");
    char *term = "TERM";
    term = getenv(term);
    if (term == NULL) print_terminal();//parent proc so not 100% way
    else printf("%s\n",term);
    printf("CPU: ");
    print_cpu();

    printf("Memory: %lu %s / %lu %s \n",mfree.val, mfree.dim, mtot.val, mtot.dim);
    return 0;
}
